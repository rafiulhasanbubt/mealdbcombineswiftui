//
//  MealViewModel.swift
//  MealDBCombineSwiftUI
//
//  Created by rafiul hasan on 4/10/21.
//

import Foundation
import Combine

class MealViewModel: ObservableObject {
    @Published var meal: Meal?
    @Published var imageurlString: String?
    private var cancellable: AnyCancellable?
    
    func fetchMeal() {
        
        cancellable = URLSession.shared.dataTaskPublisher(for: URL(string: "https://www.themealdb.com/api/json/v1/1/random.php")!)
            .receive(on: DispatchQueue.main)
            .sink{_ in} receiveValue: { data, _ in
                if let mealData = try? JSONDecoder().decode(MealData.self, from: data) {
                    self.meal = mealData.meals.first
                    self.imageurlString = mealData.meals.first?.imageUrl
                }
            }
    }
}
