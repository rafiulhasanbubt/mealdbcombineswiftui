//
//  MealListViewModel.swift
//  MealDBCombineSwiftUI
//
//  Created by rafiul hasan on 4/10/21.
//

import Foundation
import Combine

class MealListViewModel: ObservableObject {
    @Published var meals: [Meals] = []
    
    private var cancellable: AnyCancellable?
    
    init(){
        fetchMeals()
    }
    
    func fetchMeals() {
        cancellable = URLSession.shared.dataTaskPublisher(for: URL(string: "https://www.themealdb.com/api/json/v1/1/filter.php?c=Seafood")!)
            .receive(on: DispatchQueue.main)
            .sink{_ in} receiveValue: { data, _ in
                if let meals = try? JSONDecoder().decode(MealListResponse.self, from: data) {
                    self.meals = meals.mealList
                }
            }
    }
}
