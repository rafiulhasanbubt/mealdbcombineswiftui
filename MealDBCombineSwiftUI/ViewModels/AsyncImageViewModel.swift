//
//  AsyncImageViewModel.swift
//  MealDBCombineSwiftUI
//
//  Created by rafiul hasan on 4/10/21.
//

import Foundation
import SwiftUI

class AsyncImageViewModel: ObservableObject {
    @Published var image: UIImage?
    var url: URL?
    
    func loadImage() {
        guard let url = url else { return }
        URLSession.shared.dataTaskPublisher(for: url)
            .map{UIImage(data: $0.data)}
            .replaceError(with: nil)
            .receive(on: DispatchQueue.main)
            .assign(to: &$image)
    }
}
