//
//  CategoryListViewModel.swift
//  MealDBCombineSwiftUI
//
//  Created by rafiul hasan on 4/10/21.
//

import Foundation
import Combine

class CategoryListViewModel: ObservableObject {
    @Published var mealCategory: [MealCategory] = []
    
    private var cancellable: AnyCancellable?
    
    init(){
        fetchMealCategory()
    }
    
    func fetchMealCategory() {
        cancellable = URLSession.shared.dataTaskPublisher(for: URL(string: "https://www.themealdb.com/api/json/v1/1/categories.php")!)
            .receive(on: DispatchQueue.main)
            .sink{_ in} receiveValue: { data, _ in
                if let mealCate = try? JSONDecoder().decode(MealCategoryResponse.self, from: data) {
                    self.mealCategory = mealCate.categories
                }
            }
    }    
}
