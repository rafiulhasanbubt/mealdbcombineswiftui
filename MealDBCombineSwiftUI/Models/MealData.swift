//
//  MealData.swift
//  MealDBCombineSwiftUI
//
//  Created by rafiul hasan on 4/10/21.
//

import Foundation

struct MealData: Decodable {
    let meals: [Meal]
}

struct Meal: Decodable {
    let name: String
    let imageUrl: String
    let ingredients: [Ingredient]
    let instructions: String
}

struct Ingredient: Decodable, Hashable {
    let name: String
    let measure: String
}

extension Meal {
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let mealDictionary = try container.decode([String: String?].self)
        
        var index = 1
        var ingredients: [Ingredient] = []
        while let ingredient = mealDictionary["strIngredient\(index)"] as? String, let measure = mealDictionary["strMeasure\(index)"] as? String, !ingredient.isEmpty, !measure.isEmpty {
            ingredients.append(.init(name: ingredient, measure: measure))
            index += 1
        }
        self.ingredients = ingredients
        name = mealDictionary["strMeal"] as? String ?? ""
        imageUrl = mealDictionary["strMealThumb"] as? String ?? ""
        instructions = mealDictionary["strInstructions"] as? String ?? ""
    }
}
