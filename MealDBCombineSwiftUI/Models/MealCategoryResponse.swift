//
//  MealCategoryResponse.swift
//  MealDBCombineSwiftUI
//
//  Created by rafiul hasan on 4/10/21.
//

import Foundation

struct MealCategoryResponse: Decodable {
    let categories: [MealCategory]
    
    enum CodingKeys: String, CodingKey {
        case categories = "categories"
    }
}

struct MealCategory: Decodable, Hashable, Identifiable {
    let id = UUID()
    let categoryId: String
    let categoryName: String
    let categoryImage: String
    
    enum CodingKeys: String, CodingKey {
        case categoryId = "idCategory"
        case categoryName = "strCategory"
        case categoryImage = "strCategoryThumb"
    }
}
