//
//  MealListResponse.swift
//  MealDBCombineSwiftUI
//
//  Created by rafiul hasan on 4/10/21.
//

import Foundation

struct MealListResponse: Decodable {
    let mealList: [Meals]
    
    enum CodingKeys: String, CodingKey {
        case mealList = "meals"
    }
}

struct Meals: Decodable, Identifiable {
    let id = UUID()
    let mealId: String
    let mealName: String
    let imageUrl: String
    
    enum CodingKeys: String, CodingKey {
        case mealId = "idMeal"
        case mealName = "strMeal"
        case imageUrl = "strMealThumb"
    }
}
