//
//  MealDBCombineSwiftUIApp.swift
//  MealDBCombineSwiftUI
//
//  Created by rafiul hasan on 4/10/21.
//

import SwiftUI

@main
struct MealDBCombineSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
