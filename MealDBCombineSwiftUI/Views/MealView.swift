//
//  MealView.swift
//  MealDBCombineSwiftUI
//
//  Created by rafiul hasan on 4/10/21.
//

import SwiftUI

struct MealView: View {
    @StateObject private var mealVM = MealViewModel()
    
    var body: some View {
        ScrollView {
            VStack {
//                Button("Go Random Meal") {
//                    mealVM.fetchMeal()
//                }
//                .foregroundColor(.white)
//                .padding()
//                .background(Color.blue)
//                .cornerRadius(12)
//                
//                
                if let name = mealVM.meal?.name {
                    Text(name)
                        .font(.title)
                }
                
                AsyncImageView(urlString: $mealVM.imageurlString)
                
                if let ingredients = mealVM.meal?.ingredients {
                    HStack {
                        Text("Ingredients")
                            .font(.title3)
                            .fontWeight(.bold)
                        Spacer()
                    }
                    
                    ForEach(ingredients, id: \.self) { ingredient in
                        HStack {
                            Text(ingredient.name + " - " + ingredient.measure)
                            Spacer()
                        }
                    }
                }
                
                if let instructions = mealVM.meal?.instructions {
                    Text("Instructions")
                        .font(.title3)
                        .fontWeight(.bold)
                    Spacer()
                    Text(instructions)
                }
            }
            .padding()
            .onAppear {
                mealVM.fetchMeal()
            }
        }
    }
}

struct MealView_Previews: PreviewProvider {
    static var previews: some View {
        MealView()
    }
}
