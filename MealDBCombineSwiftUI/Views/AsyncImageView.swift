//
//  AsyncImageView.swift
//  MealDBCombineSwiftUI
//
//  Created by rafiul hasan on 4/10/21.
//

import SwiftUI

struct AsyncImageView: View {
    @StateObject private var imageVM = AsyncImageViewModel()
    @Binding var urlString: String?
    
    init (urlString: Binding<String?>) {
        self._urlString = urlString
    }
    
    var body: some View {
        Group {
            if let image = imageVM.image {
                Image(uiImage: image)
                    .resizable()
                    .scaledToFit()
                    .cornerRadius(16)
            } else {
                EmptyView()
                    .background(Color.gray)
            }
        }.onChange(of: urlString) { value in
            if let urlString = urlString, let url = URL(string: urlString) {
                imageVM.url = url
                imageVM.loadImage()
            }
        }
    }
}

//struct AsyncImageView_Previews: PreviewProvider {
//    static var previews: some View {
//        AsyncImageView()
//    }
//}
