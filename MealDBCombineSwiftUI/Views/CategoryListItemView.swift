//
//  CategoryListItemView.swift
//  MealDBCombineSwiftUI
//
//  Created by rafiul hasan on 4/10/21.
//

import SwiftUI

struct CategoryListItemView: View {
    var categoryImage: String
    var categoryName: String
    
    var body: some View {
        HStack {
            if let url = URL(string: categoryImage) {
                if let imageData: NSData = NSData(contentsOf: url) {
                    if let image = UIImage(data: imageData as Data) {
                        Image(uiImage: image)
                            .resizable()
                            .scaledToFit()
                            .clipped()
                            .frame(width: 100, height: 100)
                    }
                }
            } else {
                Image(systemName: "photo")
                    .resizable()
                    .scaledToFit()
                    .padding(.horizontal)
                    .frame(width: 100, height: 100)
            }
            
            Text(categoryName)
                .font(.subheadline)
                .padding()
            
            Spacer()
        }
    }
}

struct CategoryListItemView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryListItemView( categoryImage: "name", categoryName: "category name")
    }
}

