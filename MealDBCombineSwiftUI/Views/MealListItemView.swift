//
//  MealListItemView.swift
//  MealDBCombineSwiftUI
//
//  Created by rafiul hasan on 4/10/21.
//

import SwiftUI

struct MealListItemView: View {
    
    var mealImage: String
    var mealName: String
    
    var body: some View {
        HStack {
            if let url = URL(string: mealImage) {
                if let imageData: NSData = NSData(contentsOf: url) {
                    if let image = UIImage(data: imageData as Data) {
                        Image(uiImage: image)
                            .resizable()
                            .scaledToFill()
                            .clipped()
                            .frame(width: 120, height: 100)
                            .clipShape(RoundedRectangle(cornerRadius: 16))
                    }
                }
            } else {
                Image(systemName: "photo")
                    .resizable()
                    .scaledToFit()
                    .padding(.horizontal)
                    .frame(width: 100, height: 100)
            }
            
            Text(mealName)
                .font(.subheadline)
                .padding()
            
            Spacer()
        }
    }
}

struct MealListItemView_Previews: PreviewProvider {
    static var previews: some View {
        MealListItemView(mealImage: "meal image", mealName: "meal name")
    }
}
