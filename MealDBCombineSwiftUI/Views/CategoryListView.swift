//
//  CategoryListView.swift
//  MealDBCombineSwiftUI
//
//  Created by rafiul hasan on 4/10/21.
//

import SwiftUI

struct CategoryListView: View {
    @StateObject private var mealCategoryVM = CategoryListViewModel()
    
    var body: some View {
        
        NavigationView {
            ScrollView {
                VStack {
                    ForEach(mealCategoryVM.mealCategory) { category in
                        NavigationLink(destination: MealListView(categoryName: category.categoryName)) {
                            CategoryListItemView(categoryImage: category.categoryImage, categoryName: category.categoryName)
                                .padding(.horizontal)
                        }
                    }
                    .navigationTitle("Meal Category")
                }
            }
        }
    }
}

struct CategoryListView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryListView()
    }
}
