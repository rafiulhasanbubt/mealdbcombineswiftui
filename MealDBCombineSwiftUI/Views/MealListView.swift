//
//  MealListView.swift
//  MealDBCombineSwiftUI
//
//  Created by rafiul hasan on 4/10/21.
//

import Foundation
import SwiftUI

struct MealListView: View {
    @StateObject private var mealsVM = MealListViewModel()
    @State var categoryName: String
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack {
                    ForEach(mealsVM.meals) { meal in
                        NavigationLink(destination: MealView()) {
                            MealListItemView(mealImage: meal.imageUrl, mealName: meal.mealName).padding(.horizontal)
                        }
                    }
                    .navigationTitle(categoryName)
                    .navigationBarTitleDisplayMode(.inline)
                }
            }
        }
    }
}

struct MealListView_Previews: PreviewProvider {
    static var previews: some View {
        MealListView(categoryName: ".constant")
    }
}
